import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by omfg on 10.12.16.
 */
public class DBWorker {
    private final String HOST = "jdbc:postgresql://192.168.1.106:5432/testdb";
    private final String USERNAME = "omfg";
    private final String PASSWORD = "101541";

    private Connection connection;


    public DBWorker() {
        try {
            connection = DriverManager.getConnection(HOST,USERNAME,PASSWORD);
        }catch (SQLException e){e.printStackTrace();
        }
    }

    public Connection getConnection() {
        return connection;
    }
}
